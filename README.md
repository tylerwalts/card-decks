#card-decks
This is a microservice to create, show, shuffle and destroy named decks of playing cards.

*Assumptions:*

* Cards are standard 52-card decks with heart, spade, diamond and club suits.
* Each suit contains a card for A, 2, 3, 4, 5, 6, 7, 8, 9, 10, J, Q and K.
* There are no Jokers.

*Service Overview:*

* Users may create unbounded numbers of named decks.
* Users may shuffle decks by name.
* Users may delete decks by name.
* Usage is unidentified and unsecured.  (This service is part of a non-compliant gambling operation)
* If the deck service host is bounced, all decks are destroyed. (Step 1 of [2](https://www.youtube.com/watch?v=yJldu7JGoAw)-step response to police raid).

#Development Notes
This service application is developed using Java, [Spring Boot](http://docs.spring.io/spring-boot/docs/1.4.0.RELEASE/reference/htmlsingle/) and Gradle.

#Usage
The following commands will require a recent version of and familiarity with:

* Gradle
* Java

##Test
From the project root, run:
`gradle test`

##Build
From the project root, run:
`gradle build`

* The compiled jar will be in the ./build directory

##Run
From the project root, run:
`java -jar build/libs/card-decks-0.1.0.jar`

* The service will run on port 8080, and output will go to the console.
* Hit [ctrl-c] to stop.

##Verification
From the browser:

* Open http://localhost/
    * Expect to see the root doc in the browser

From command line:
```bash
# Base URL to list endpoint:
curl localhost:8080

# List all Decks:
curl localhost:8080/decks

# Create a new Deck:
curl -X POST -H "Content-Type:application/json" -d '{ "name":"MyDeck"}' localhost:8080/decks

# Shuffle the cards in a Deck:
curl -X PUT localhost:8080/decks/MyDeck

# Delete a Deck:
curl -X DELETE localhost:8080/decks/MyDeck
```

#TODO
The following features are being tracked:

* Implement a complex shuffling algorithm that simulates splitting decks.
* Add java linter via gradle.
* Add deployment job to gradle.

See [issues list](https://bitbucket.org/tylerwalts/card-decks/issues?status=new&status=open) for open tasks.

#Author
Tyler Walters
tyler@tylerwalts.com
