package decks;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;
import java.net.URL;
import java.net.URI;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.http.HttpMethod;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DecksIntegrationTest {

    @LocalServerPort
    private int port;

    private URL base;

    // Hateos resource types
    private ParameterizedTypeReference<Resource<Deck>> singleDeckResponseType = new ParameterizedTypeReference<Resource<Deck>>() {};
    private ParameterizedTypeReference<Resource<Deck[]>> deckListResponseType = new ParameterizedTypeReference<Resource<Deck[]>>() {};

    @Autowired
    private TestRestTemplate template;

    @Before
    public void setUp() throws Exception {
        this.base = new URL("http://localhost:" + port + "/");
    }

    @Test
    public void RootURLReturnsBody() throws Exception {
        ResponseEntity<String> response = template.getForEntity(base.toString(), String.class);
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertThat(response.getBody(), containsString("decks"));
    }

    @Test
    public void CreateAndGetDeckWithDefaultCards() throws Exception {
        // Create
        URI newDeckURI = template.postForLocation(base.toString() + "decks", new Deck("DeckWithDefaultCards"));
        System.out.println("URI: " + newDeckURI);
        assertEquals(newDeckURI.getPath(), "/decks/DeckWithDefaultCards");

        // Get
        ResponseEntity<Resource<Deck>> response = template.exchange(base.toString() + "decks/DeckWithDefaultCards", HttpMethod.GET, null, singleDeckResponseType);
        Deck deck = response.getBody().getContent();
        System.out.println("deck: " + deck.toString());
        assertEquals(deck.getCards(), deck.defaultCardOrder());
    }

    @Test
    public void CreateTwoAndListDecks() throws Exception {
        // Create
        URI newDeckURI1 = template.postForLocation(base.toString() + "decks", new Deck("DeckForList1"));
        URI newDeckURI2 = template.postForLocation(base.toString() + "decks", new Deck("DeckForList2"));
        System.out.println("URI1: " + newDeckURI1 + ", URI2: " + newDeckURI2);
        assertEquals(newDeckURI1.getPath(), "/decks/DeckForList1");
        assertEquals(newDeckURI2.getPath(), "/decks/DeckForList2");

        // Get List
        ResponseEntity<Resource<Deck[]>> response = template.exchange(base.toString() + "decks", HttpMethod.GET, null, deckListResponseType);
        HttpStatus statusCode = response.getStatusCode();
        System.out.println("statusCode: " + statusCode);
        assertEquals(statusCode, HttpStatus.OK);
    }

    @Test
    public void CreateAndDeleteDeck() throws Exception {
        // Create
        URI newDeckURI = template.postForLocation(base.toString() + "decks", new Deck("DeckToDelete"));
        System.out.println("URI: " + newDeckURI);
        assertEquals(newDeckURI.getPath(), "/decks/DeckToDelete");

        // Delete
        template.delete(base.toString() + "decks/DeckToDelete");

        // Verify not found
        ResponseEntity<Resource<Deck>> response = template.exchange(base.toString() + "decks/DeckToDelete", HttpMethod.GET, null, singleDeckResponseType);
        HttpStatus statusCode = response.getStatusCode();
        System.out.println("statusCode: " + statusCode);
        assertEquals(statusCode, HttpStatus.NOT_FOUND);
    }

    @Test
    public void CreateAndShuffleDeck() throws Exception {
        // Create
        URI newDeckURI = template.postForLocation(base.toString() + "decks", new Deck("DeckToShuffle"));
        System.out.println("URI: " + newDeckURI);
        assertEquals(newDeckURI.getPath(), "/decks/DeckToShuffle");

        // Shuffle & Verify the card order has changed
        ResponseEntity<Resource<Deck>> response = template.exchange(newDeckURI, HttpMethod.PUT, null, singleDeckResponseType);
        HttpStatus statusCode = response.getStatusCode();
        System.out.println("statusCode: " + statusCode);
        assertEquals(statusCode, HttpStatus.OK);
        Deck deck = response.getBody().getContent();
        System.out.println("deck: " + deck.toString());
        assertNotEquals(deck.getCards(), deck.defaultCardOrder());
    }
}
