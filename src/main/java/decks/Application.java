package decks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {
    private static String server = "localhost";
    private static String port = "8080";

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
        System.out.println("Card Decks Service has been started at: http://" + server + ":" + port);
        System.out.println("_____ Example Usage _____");
        System.out.println("curl " + server + ":" + port);
        System.out.println("curl " + server + ":" + port + "/decks");
        System.out.println("curl -X POST -H \"Content-Type:application/json\" -d '{ \"name\":\"MyDeck\"}' " + server + ":" + port + "/decks");
        System.out.println("curl -X PUT -H \"Content-Type:application/json\" -d '{ \"cards\":\"\"}' " + server + ":" + port + "/decks/MyDeck");
        System.out.println("curl -X PUT -H \"Content-Type:application/json\" -d '{ \"cards\":\"NEWCARDS\"}' " + server + ":" + port + "/decks/MyDeck");
        System.out.println("curl -X DELETE " + server + ":" + port + "/decks/MyDeck");
        System.out.println("curl -X PUT " + server + ":" + port + "/decks/MyDeck");
    }

}
