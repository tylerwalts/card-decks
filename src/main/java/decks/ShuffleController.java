package decks;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.hateoas.Resource;

@BasePathAwareController
public class ShuffleController {

    private final DeckRepository deckRepo;
    public ShuffleController(DeckRepository deckRepo) {
        this.deckRepo = deckRepo;
    }

    @RequestMapping(value = "/decks/{name}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Resource<Deck>> shuffleDeck(@PathVariable String name) {
        try {
            Deck deck = deckRepo.findOne(name);
            System.out.println("Found deck: " + deck.toString());
            deck.shuffle();
            deckRepo.save(deck);
            System.out.println("Shuffled deck: " + deck.toString());
            Resource<Deck> deckResource = new Resource<Deck>(deck);
            return ResponseEntity.ok(deckResource);
        } catch (Exception e) {
            System.out.println("Deck Not found: " + name);
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

}
