package decks;

import java.util.*;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Deck {

    @Id
    private String name;
    // Store cards as a ~160char CSV string for MVP
    private String cards;
    private final static String DEFAULT_CARD_ORDER = "AH,2H,3H,4H,5H,6H,7H,8H,9H,10H,JH,QH,KH,AS,2S,3S,4S,5S,6S,7S,8S,9S,10S,JS,QS,KS,AD,2D,3D,4D,5D,6D,7D,8D,9D,10D,JD,QD,KD,AC,2C,3C,4C,5C,6C,7C,8C,9C,10C,JC,QC,KC";

    protected Deck () {
        this.cards = DEFAULT_CARD_ORDER;
    }

    public Deck(String name) {
        this.name = name;
        this.cards = DEFAULT_CARD_ORDER;
    }

    public Deck(String name, String cards) {
        this.name = name;
        this.cards = cards;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCards() {
        return this.cards;
    }

    public void setCards(String cards) {
        if (cards != null && cards.length() > 0) {
            this.cards = cards;
        } else {
            this.cards = DEFAULT_CARD_ORDER;
        }
    }

    public void shuffle() {
        ArrayList<String> cardArray = new ArrayList<String>(Arrays.asList(this.cards.split(",")));
        Collections.shuffle(cardArray);
        this.cards = String.join(",", cardArray);
    }

    public String defaultCardOrder() {
        return DEFAULT_CARD_ORDER;
    }

    public String toString() {
        return "Deck[ cards: " + this.cards + " ]";
    }
}
