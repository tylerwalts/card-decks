package decks;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "decks", path = "decks")
public interface DeckRepository extends CrudRepository<Deck, String> {

}
